package com.bbs.techtest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Diff<T extends Serializable> {

	private Map<String,List<Object>> diffs = new LinkedHashMap<String,List<Object>>();

	public Map<String, List<Object>> getDiffs() {
		return diffs;
	}

	public void setDiffs(Map<String, List<Object>> diffs) {
		this.diffs = diffs;
	}
		
	public String toString() {
			
		return loopThroughMap(diffs);
	}
	
	@SuppressWarnings("unchecked")
	private  String loopThroughMap(Map<String,List<Object>> map) {
		
		String prefix = "1";
		StringBuffer buf = new StringBuffer(); 
		int idx = 1;
		
		ValuesWithDiff diff = null;
		Map<String,Object> innerMap = null;
		
		for(Map.Entry<String,List<Object>> entry : map.entrySet()) {
			
			List<Object> objs = entry.getValue();
			
			for(Object o : objs) {
				if(o instanceof ValuesWithDiff) {
					diff = (ValuesWithDiff) o;
					buf.append(formatData(entry.getKey(),diff,prefix,true));
					prefix = splitStrAndIncr(prefix);
				}else {
					innerMap = (Map<String,Object>) o;
					
					idx = 1;
					for(Map.Entry<String,Object> innerEntry : innerMap.entrySet()) {
						buf.append(formatData(innerEntry.getKey(),(ValuesWithDiff)innerEntry.getValue(),prefix + "." + idx,false));
						idx++;
					}
					prefix = prefix + "." + idx;
				}
			}
		}
		
		return buf.toString();
	}
	
	private String splitStrAndIncr(String val) {
		
		String [] vals = val.split(".");
		
		if(vals.length < 2) {
			return val;
		}
		
		StringBuffer sb = new StringBuffer();
		
		for(int i = 0; i < vals.length; i++) {
			if(i == (vals.length - 1)) {
				int v = Integer.valueOf(vals[i]);
				sb.append(++v);
			} else {
				sb.append(vals[i] + ".");
			}
		}
		
		return sb.toString();
	}
	
	private String formatData(String name, ValuesWithDiff val, String prefix, boolean isTopLevel) {
		
		StringBuffer sb = new StringBuffer();
		
		DiffOps ops = val.getDiffOps();
		
		if(isTopLevel) {
			sb.append(prefix + " " + ops + ":" + name + "\n");
			return sb.toString();
		}
		
		if(ops.equals(DiffOps.CREATE)) {
			sb.append(prefix  + " " + ops + ":" + name + " as " + "'" + val.getAnother() + "'");
		}else if(ops.equals(DiffOps.DELETE)) {
			sb.append(prefix + " " + ops + ":" + name);
		}else {
			sb.append(prefix + " " + ops + ":" + name + " from " + "'" + val.getOrg() + "'" + " to " + "'" + val.getAnother() + "'");
		}
		
		sb.append("\n");
		return sb.toString();
	}

	public void serialize(T obj, String fileName) {

		try {    

			FileOutputStream file = new FileOutputStream(fileName); 
			ObjectOutputStream out = new ObjectOutputStream(file); 

			out.writeObject(obj); 

			out.close(); 
			file.close(); 
		} catch(IOException ex) { 
			System.out.println("IOException is caught during Serialization");
		}
	}

	@SuppressWarnings("unchecked")
	public T deserialize(String fileName) {

		T obj = null;
		
		try {    

			FileInputStream file = new FileInputStream(fileName); 
			ObjectInputStream in = new ObjectInputStream(file); 

			obj = (T)in.readObject();

			in.close(); 
			file.close();
		} catch(IOException ex) {
			System.out.println("IOException is caught during DeSerialization"); 
		} 
		catch(ClassNotFoundException ex) { 
			System.out.println("ClassNotFoundException is caught during DeSerialization"); 
		} 

		return obj;
	}
}
