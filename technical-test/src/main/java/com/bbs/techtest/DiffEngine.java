package com.bbs.techtest;

import java.io.Serializable;

public interface DiffEngine {
	
	<T extends Serializable> T apply(T original, Diff<?> diff) throws DiffException;
	
	<T extends Serializable> Diff<T> calculate(T original, T modified) throws DiffException;
}
