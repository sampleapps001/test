package com.bbs.techtest;

public class ValuesWithDiff {

	private Object org;
	private Object another;
	private DiffOps diffOps;

	public Object getOrg() {
		return org;
	}
	
	public void setOrg(Object org) {
		this.org = org;
	}
	
	public Object getAnother() {
		return another;
	}
	
	public void setAnother(Object another) {
		this.another = another;
	}
	
	public DiffOps getDiffOps() {
		return diffOps;
	}
	
	public void setDiffOps(DiffOps diffOps) {
		this.diffOps = diffOps;
	}
	
	public String toString() {
		
		String orgStr = " ";
		String modifiedStr = " ";
		
		if(org != null) {
			orgStr = org.toString();
		}
		
		if(another != null) {
			modifiedStr = another.toString();
		}
		
		return "Original: " + orgStr + " Another: " + modifiedStr + " Difference: " + diffOps.toString();
	}
}
