package com.bbs.techtest;

public interface DiffRenderer {

	public String render(Diff<?> diff) throws DiffException;
	
}
