package com.bbs.techtest;

public class DiffException extends Exception {

	private static final long serialVersionUID = -7698912729249813850L;
	
	DiffException(String msg){  
		super(msg);  
	}  

}
