package com.bbs.techtest;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DiffEngineImpl implements DiffEngine {

	private Map<String, List<Object>> diffs = new LinkedHashMap<String, List<Object>>(); 

	public <T extends Serializable> T apply(T original, Diff<?> diff) throws DiffException {
		return null;
	}

	public <T extends Serializable> Diff<T> calculate(T original, T modified) throws DiffException {

		boolean compatible = checkForCompatibility(original,modified);
		if(compatible)
			try {
				diffs.clear();
				getFieldsDiff(original,modified, diffs);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		Diff<T> diff = new Diff<T>();
		diff.setDiffs(diffs);

		return diff;
	}

	@SuppressWarnings("unchecked")
	private void getFieldsDiff(Object org, Object another, Map<String,List<Object>> finalMap) throws DiffException, IllegalArgumentException, IllegalAccessException {

		Map<String,ValuesWithDiff> map = null;
		DiffOps diffOps = null;
		ValuesWithDiff valsWithDiff = null;

		Class<?> orgClass = null; 
		Class<?> anotherClass = null;
		String simpleName = null;

		diffOps = compare(org,another);
		if(!diffOps.equals(DiffOps.EQUAL)) {

			map = new LinkedHashMap<String,ValuesWithDiff>();
			valsWithDiff = new ValuesWithDiff();

			if(diffOps.equals(DiffOps.DELETE)) {
				valsWithDiff.setDiffOps(DiffOps.DELETE);
			} else if(diffOps.equals(DiffOps.CREATE)) {
				map = newObject(another);
				valsWithDiff.setDiffOps(DiffOps.CREATE);
			} else {
				valsWithDiff.setDiffOps(DiffOps.UPDATE);
			}

			if(another != null) {
				anotherClass = another.getClass();
				orgClass = anotherClass;
			}

			if(org != null) {
				orgClass = org.getClass();
				anotherClass = orgClass;
			}

			List<Object> list = new ArrayList<Object>();
			
			simpleName = orgClass.getSimpleName();
			List<Object> existingList = null;
			if(finalMap.containsKey(simpleName)) {
				existingList = finalMap.get(simpleName);
			}
				
			list.add(valsWithDiff);
			if(diffOps.equals(DiffOps.CREATE)) {
				list.add(map);
			}
			
			if(existingList != null) {
				existingList.addAll(list);
				finalMap.put(simpleName, existingList);
			} else {
				finalMap.put(simpleName, list);
			}
				
			if(diffOps.equals(DiffOps.CREATE) || diffOps.equals(DiffOps.DELETE)) {
				return;
			}
			
		} else {
			System.out.println("Both the objects are equal");
			return;
		}

		Field[] fields = orgClass.getDeclaredFields();
		Field[] anotherFields = anotherClass.getDeclaredFields();

		String name = null;
		Object orgValue = null;
		Object anotherValue = null;
		ValuesWithDiff valDiff = null;
		
		Field field = null;
		Field anotherField = null;

		int noOfFields = fields.length;
		int idx = 0;
		boolean isShallow = false;
		while(idx < noOfFields) {

			field = fields[idx];
			anotherField = anotherFields[idx];

			field.setAccessible(true);
			anotherField.setAccessible(true);

			name = field.getName();
			
			isShallow = isShallow(field.getType());
			if(isShallow) {
				valDiff = getDiff(field,anotherField,org,another);
				boolean isNotFirstTime = false;
				if(valDiff != null) {
					List<Object> existingList = null;
					if(finalMap.containsKey(simpleName)) {
						existingList = finalMap.get(simpleName);
						for(Object o:existingList) {
							if(o instanceof Map<?, ?>) {
								((Map<String,Object>)o).put(name, valDiff);
								isNotFirstTime = true;
								break;
							}
						}
						if(!isNotFirstTime) {
							Map<String,Object> newMap = new LinkedHashMap<String,Object>();
							newMap.put(name,valDiff);
							existingList.add(newMap);
							finalMap.put(simpleName, existingList);
						}
					}	
				}
			} else {
				orgValue = field.get(org);
				anotherValue = anotherField.get(another);
				diffOps = compare(orgValue,anotherValue);
				if(diffOps.equals(DiffOps.EQUAL)) {
					return;
				}
				getFieldsDiff(orgValue,anotherValue,finalMap);
			}

			idx++;
		}

	}

	private ValuesWithDiff getDiff(Field orgField,Field anotherField, Object org, Object another) throws IllegalArgumentException, IllegalAccessException {

		Object orgValue = orgField.get(org);
		Object anotherValue = anotherField.get(another);

		ValuesWithDiff valsWithDiff = null;

		if(orgValue != null && !orgValue.equals(anotherValue)) {
			valsWithDiff = new ValuesWithDiff();
			valsWithDiff.setOrg(orgValue);
			valsWithDiff.setAnother(anotherValue);
			valsWithDiff.setDiffOps(DiffOps.UPDATE);
		}

		return valsWithDiff;
	}

	private Map<String,ValuesWithDiff> newObject(Object obj) throws IllegalArgumentException, IllegalAccessException {

		Class<?> clazz = obj.getClass(); 
		Field[] fields = clazz.getDeclaredFields();

		Map<String,ValuesWithDiff> map = null;
		ValuesWithDiff valsWithDiff = null;

		for(Field field:fields) {
			map = new LinkedHashMap<String,ValuesWithDiff>();
			String name = field.getName();
			Object value = field.get(obj);

			valsWithDiff = new ValuesWithDiff();
			valsWithDiff.setOrg(null);
			valsWithDiff.setAnother(value);
			valsWithDiff.setDiffOps(DiffOps.CREATE);

			map.put(name, valsWithDiff);
		}

		return map;
	}

	@SuppressWarnings("rawtypes")
	private boolean isShallow(Class type) {

		if(String.class.isAssignableFrom(type) || 
				type.isPrimitive() ||
				type.isArray() ||
				Iterable.class.isAssignableFrom(type)) {
			
			return true;
		}

		return false;
	}

	private DiffOps compare(Object org, Object another) {

		if(org == null && another == null) {
			return DiffOps.EQUAL;
		} else if(org == null) {
			return DiffOps.CREATE;
		} else if(another == null) {
			return DiffOps.DELETE;
		} else {
			if(org.equals(another)) {
				return DiffOps.EQUAL;
			} else {
				return DiffOps.UPDATE;
			}	
		}
	}

	private boolean checkForCompatibility(Object obj, Object another) {

		if(!obj.getClass().equals(another.getClass())) {
			return false;
		}

		return true;
	}

}
