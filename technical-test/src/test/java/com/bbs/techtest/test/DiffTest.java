package com.bbs.techtest.test;

import junit.framework.*;
import org.junit.Test;

import com.bbs.techtest.Diff;
import com.bbs.techtest.DiffEngine;
import com.bbs.techtest.DiffEngineImpl;
import com.bbs.techtest.DiffException;
import com.bbs.techtest.Person;
import com.bbs.techtest.Pet;

import java.util.HashSet;
import java.util.Set;

public class DiffTest extends TestCase {
	
	@Test
    public void testCheckDifferences() throws DiffException {
		
		DiffEngine diffEngine = new DiffEngineImpl();
		
		Pet pet = new Pet();
		pet.setName("Tommy");
		pet.setType("Dog");
		Set<Integer> set = new HashSet<Integer>();
		set.add(10);
		set.add(20);

		Pet anotherPet = new Pet();
		anotherPet.setName("Tom");
		anotherPet.setType("Cat");
		Set<Integer> set1 = new HashSet<Integer>();
		set1.add(10);
		set1.add(20);

		Person p = new Person();
		p.setFirstName("Srini");
		p.setSurname("Ch");
		p.setFriend(null);
		p.setNickNames(new HashSet<String>());
		p.setPet(pet);

		Person p1 = new Person();
		p1.setFirstName("Srinivas");
		p1.setSurname("Ch");
		p1.setFriend(null);
		p1.setNickNames(new HashSet<String>());
		p1.setPet(anotherPet);

		Diff<Person> diff = diffEngine.calculate(p, p1);
		
		assertEquals(diff.getDiffs().toString(),"{Person=[Original:   Another:   Difference: UPDATE, {firstName=Original: Srini Another: Srinivas Difference: UPDATE}], Pet=[Original:   Another:   Difference: UPDATE, {type=Original: Dog Another: Cat Difference: UPDATE, name=Original: Tommy Another: Tom Difference: UPDATE}]}");
    }
}

